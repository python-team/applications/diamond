Source: diamond
Section: utils
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 11), python, dh-python, python-setuptools, python-configobj, python-psutil
Standards-Version: 4.4.1
Homepage: https://github.com/python-diamond/Diamond
Vcs-Git: https://salsa.debian.org/python-team/packages/diamond.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/diamond

Package: diamond
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, adduser, python-diamond (= ${binary:Version}), python-configobj
Suggests: python-psutil, lsb-base (>= 3.0-6)
Description: smart data producer for Graphite graphing package
 Diamond is a python daemon that collects system metrics and publishes them to
 Graphite (and others). It is capable of collecting cpu, memory, network, i/o,
 load and disk metrics. Additionally, it features an API for implementing custom
 collectors for gathering metrics from almost any source.
 .
 This package contains the daemon and its configuration and accessories.

Package: python-diamond
Architecture: all
Section: python
Depends: ${python:Depends}, ${misc:Depends}, python-configobj, python-pkg-resources
Description: smart data producer for Graphite graphing package (Python module)
 Diamond is a python daemon that collects system metrics and publishes them to
 Graphite (and others). It is capable of collecting cpu, memory, network, i/o,
 load and disk metrics. Additionally, it features an API for implementing custom
 collectors for gathering metrics from almost any source.
 .
 This package contains the 'diamond' python module.
