Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: diamond
Source: https://github.com/python-diamond/Diamond

Files: *
Copyright:
 Copyright (C) 2010-2012 by Brightcove Inc.      http://www.brightcove.com/
 Copyright (C) 2011-2012 by Ivan Pouzyrevsky
 Copyright (C) 2011-2012 by Rob Smith            http://www.kormoc.com
 Copyright (C) 2012 Wijnand Modderman-Lenstra    https://maze.io/
 Copyright (C) 2012 Dennis Kaarsemaker           <dennis@kaarsemaker.net>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Files: debian/*
Copyright: 2016-2019 Sandro Tosi <morph@debian.org>
License: MIT

Files: debian/diamond.init
Author: Ivan Pouzyrevsky <sandello@yandex-team.ru>
License: MIT

Files: src/diamond/gmetric.py
Copyright: Copyright (c) 2007,2008 Nick Galbreath
License: MIT

Files: src/collectors/onewire/onewire.py
Author: Tomasz Prus

Files: src/collectors/jbossapi/jbossapi.py
Comment:
 Much of the code was borrowed from:
 .
 http://bit.ly/XRrCWx
 https://github.com/lukaf/munin-plugins/blob/master/jboss7_

Files: src/collectors/jcollectd/collectd_network.py
Copyright: Copyright © 2009 Adrian Perez <aperez@igalia.com>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: src/collectors/endecadgraph/endecadgraph.py
Authors: Jan van Bemmelen <jvanbemmelen@bol.com>
 Renzo Toma <rtoma@bol.com>
